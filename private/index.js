const jwt = require("jwt-simple");
const secret = "4dm1s1oN";
const RegistroDB = require("../db/Registros");
const Admision = require("../db/Admision");

const searchInstitucion = (request, response, next)=>{
    var institucion = request.body.institucion;
    var size = Admision.formularios.length;

    for (let i = 0; i < size; i++) {
        if(Admision.formularios[i].institucion==institucion){
            response.search = Admision.formularios[i];
        }     
    }
    next();
}

module.exports = (app) => {
	app.post('/registro', searchInstitucion, (request, response)=>{
    try{
      const newReg = new RegistroDB(request.body);
      newReg.save((err) => {
        if(err){
          return response.send({ error: 403 });
        }
        return response.send({ success: "Ok" });
      });
    }catch(e){
      return response.send({ error: e });
    }
  });
}
