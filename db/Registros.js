const mongoose = require("mongoose");

const DetalleTutor = new mongoose.Schema({
	tutor_nombre: String,
	tutor_telefono: String,
	tutor_correo: String,
	tutor_ocupacion: String,
	tutor_telefono_trabajo: String,
	tutor_parentesco: String,
	tutor_domicilio: String,
	tutor_curp: String,
	tutor_clave_elector: String
}, { timestamps: true, strict: false })

const Registros = new mongoose.Schema({
	nivel_escolar: String,
	programa_educativo: String,
	plantel: String,
	modalidad: String,
	periodo: String,
	nacionalidad: String,
	estado_nacimiento: String,
	estado_civil: String,
	cp_nacimiento: String,
	localidad_nacimiento: String,
	municipio_nacimiento: String,
	DetalleTutor: [ DetalleTutor ],
	laborando: String,
	beca: String,
	tipo_sanguineo: String,
	tipo_discapacidad: String,
	lengua: String,
	procedencia_internacional: String,
	nombre_escuela_internacional: String,
	direccion_escuela_internacional: String,
	estado_procedencia: String,
	municipio_procedencia: String,
	localidad_procedencia: String,
	escuela_procedencia: String,
	ingreso: String,
	egreso: String,
	promedio: Number,
	especialidad: String,
	sostenimiento: String,
	medio: String,
	decision: String,
	calificacion: String,
	otro_medio: String,
	acta_file: String,
	constancia_file: String,
	carta_motivos_file: String,
	fotografía_file: String,
	curp_file: String,
	carta_conducta_file: String
}, { timestamps: true, strict: false });

module.exports = mongoose.model("registros", Registros);