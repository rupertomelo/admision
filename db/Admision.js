module.exports = {
    "formularios": [
      {
        "institucion": "Instituto Tecnológico Superior de Huichapan",
        "etapas": ["Bienvenida", "Cuestionario", "Documentos", "Bancos", "Pase"],
        "max_reg":1000,
        "pasos":[
          {
            "paso": "Bienvenida",
            "texto": "Bienvenido a ITESHU"
          },
          {
            "paso": "Cuestionario",
            "preguntas":[
              {
                "pregunta":"¿A que nivel escolar deseas entrar?",
                "type":"select",
                "options": ["Licenciatura", "Ingeniería", "Maestría"],
                "active": true,
                "required": true,
                "id": "nivel_escolar",
                "dependencia":""
              },
              {
                "pregunta":"¿Seleccione el programa educativo?",
                "type":"select",
                "options": [
                  {
                    "nivel_escolar": "Licenciatura",
                    "programa_educativo":["Arquitectura", "Gastronomía"]
                  },
                  {
                    "nivel_escolar": "Ingeniería",
                    "programa_educativo":["Administración", "Sistemas computacionales", "Mecatrónica", "Energías renovables", "Industrial", "Innovación agrícola sustentable", "Gestión empresarial"]
                  },
                  {
                    "nivel_escolar": "Maestría",
                    "programa_educativo":["Mecatrónica"]
                  }
                ],
                "active": true,
                "required": false,
                "id": "programa_educativo",
                "dependencia":"nivel_escolar"
              },
              {
                "pregunta":"¿Seleccione el plantel?",
                "type":"select",
                "options": [
                  {
                    "programa_educativo": "Arquitectura",
                    "plantel":["Huichapan"]
                  },
                  {
                    "programa_educativo": "Gastronomía",
                    "plantel":["Huichapan", "Tecozautla"]
                  },
                  {
                    "programa_educativo": "Administración",
                    "plantel":["Huichapan"]
                  },
                  {
                    "programa_educativo": "Sistemas computacionales",
                    "plantel":["Huichapan", "Tecozautla"]
                  },
                  {
                    "programa_educativo": "Mecatrónica",
                    "plantel":["Huichapan"]
                  },
                  {
                    "programa_educativo": "Energías renovables",
                    "plantel":["Huichapan", "Tecozautla"]
                  },
                  {
                    "programa_educativo": "Industrial",
                    "plantel":["Huichapan"]
                  },
                  {
                    "programa_educativo": "Innovación agrícola sustentable",
                    "plantel":["Huichapan", "Tecozautla"]
                  },
                  {
                    "programa_educativo": "Gestión empresarial",
                    "plantel":["Huichapan", "Tecozautla"]
                  }
                ],
                "active": true,
                "required": false,
                "id": "plantel",
                "dependencia":"programa_educativo"
              },
              {
                "pregunta":"¿Seleccione la modalidad?",
                "type":"select",
                "options": [
                  {
                    "programa_educativo":"Arquitectura",
                    "plantel": "Huichapan",
                    "modalidad":["Escolarizada"]
                  },
                  {
                    "programa_educativo":"Gastronomía",
                    "plantel": "Huichapan",
                    "modalidad":["Escolarizada", "Mixta"]
                  },
                  {
                    "programa_educativo":"Gastronomía",
                    "plantel": "Tecozautla",
                    "modalidad":["Escolarizada"]
                  },
                  {
                    "programa_educativo":"Sistemas computacionales",
                    "plantel": "Tecozautla",
                    "modalidad":["Escolarizada"]
                  },
                  {
                    "programa_educativo":"Mecatrónica",
                    "plantel": "Huichapan",
                    "modalidad":["Escolarizada"]
                  }
                ],
                "active": true,
                "required": false,
                "id": "modalidad",
                "dependencia":"programa_educativo",
                "dependencia2":"plantel"
              },
              {
                "pregunta":"¿Seleccione el período?",
                "type":"select",
                "options": [
                  {
                    "programa_educativo":"Arquitectura",
                    "plantel": "Huichapan",
                    "periodo":["Enero-Julio","Agosto-Diciembre"]
                  },
                  {
                    "programa_educativo":"Arquitectura",
                    "plantel": "Huichapan",
                    "periodo":["Enero-Abril","Mayo-Agosto","Septiembre-Diciembre"]
                  },
                  {
                    "programa_educativo":"Gastronomía",
                    "plantel": "Huichapan",
                    "periodo":["Enero-Julio","Agosto-Diciembre"]
                  },
                  {
                    "programa_educativo":"Gastronomía",
                    "plantel": "Tecozautla",
                    "periodo":["Enero-Abril","Mayo-Agosto","Septiembre-Diciembre"]
                  },
                  {
                    "programa_educativo":"Sistemas computacionales",
                    "plantel": "Tecozautla",
                    "periodo":["Enero-Abril","Mayo-Agosto","Septiembre-Diciembre"]
                  },
                  {
                    "programa_educativo":"Mecatrónica",
                    "plantel": "Huichapan",
                    "periodo":["Enero-Abril","Mayo-Agosto","Septiembre-Diciembre"]
                  }
                ],
                "active": true,
                "required": false,
                "id": "periodo",
                "dependencia":"programa_educativo",
                "dependencia2":"plantel"
              },
              {
                "section":"Datos de nacimiento",
                "pregunta": "Nacionalidad",
                "type": "select",
                "options": ["Mexicana","Extranjera"],
                "active": true,
                "required": false,
                "id": "nacionalidad",
                "dependencia":""
              },
              {
                "type":"api",
                "nombre":"direccion",
                "estado":"estado_nacimiento",
                "municipio":"municipio_nacimiento",
                "cp":"cp_nacimiento",
                "localidad":"localidad_nacimiento",
                "dependencia":"nacionalidad",
                "active":true,
                "value":"Mexicana"
              },
              {
                "pregunta": "¿Estado civil?",
                "type": "select",
                "options": ["Soltero(a)", "Casado(a)", "Viudo(a)", "Divorciado(a)", "Union Libre"],
                "active": true,
                "required": false,
                "id": "estado_civil",
                "dependencia":""
              },
              { 
                "type": "array",
                "id": "DetalleTutor",
                "dependencia":"",
                "active": true,
                "required": false,
                "dependencia":"",
                "multiObjects": [
                  {
                    "title": "Registre información de tutor(es)",
                    "id": "DetalleTutor",
                    "schema": [
                      {
                        "pregunta": "¿Nombre completo del padre o tutor?",
                        "type": "text",
                        "options": [],
                        "active": true,
                        "required": false,
                        "id":"tutor_nombre",
                        "dependencia":""
                      },
                      {
                        "pregunta": "Número telefónico",
                        "type": "text",
                        "options": [],
                        "active": true,
                        "required": false,
                        "id":"tutor_telefono",
                        "dependencia":""
                      },
                      {
                        "pregunta": "Correo electónico",
                        "type": "email",
                        "options": [],
                        "active": true,
                        "required": false,
                        "id":"tutor_correo",
                        "dependencia":""
                      },
                      {
                        "pregunta": "¿Ocupación?",
                        "type": "text",
                        "options": [],
                        "active": true,
                        "required": false,
                        "id":"tutor_ocupacion",
                        "dependencia":""
                      },
                      {
                        "pregunta": "Teléfono del trabajo",
                        "type": "text",
                        "options": [],
                        "active": true,
                        "required": false,
                        "id":"tutor_telefono_trabajo",
                        "dependencia":""
                      },
                      {
                        "pregunta": "Parentesco",
                        "type": "select",
                        "options": ["Padre", "Madre", "Tutor"],
                        "active": true,
                        "required": false,
                        "id":"tutor_parentesco",
                        "dependencia":""
                      },
                      {
                        "pregunta": "Domicilio",
                        "type": "text",
                        "options": [],
                        "active": true,
                        "required": false,
                        "id":"tutor_domicilio",
                        "dependencia":""
                      },
                      {
                        "pregunta": "Curp",
                        "type": "text",
                        "options": [],
                        "active": true,
                        "required": false,
                        "id":"tutor_curp",
                        "dependencia":""
                      },
                      {
                        "pregunta": "Clave de elector",
                        "type": "text",
                        "options": [],
                        "active": true,
                        "required": false,
                        "id":"tutor_clave_elector",
                        "dependencia":""
                      }
                    ]
                  }
                ] 
              },
              {
                "pregunta": "¿Actualmente se encuentra laborando?",
                "type": "select",
                "options": ["Si", "No"],
                "active": true,
                "required": false,
                "id": "laborando",
                "dependencia":""
              },
              {
                "pregunta": "¿Actualmente cuenta con beca?",
                "type": "select",
                "options":[
                  {
                    "nivel_escolar": "Ingeniería",
                    "beca":["Ninguna","beca ingeniaria 1", "beca ingeniaria 2"]
                  },
                  {
                    "nivel_escolar":"Licenciatura",
                    "beca":["Ninguna","beca licenciatura 1", "beca licenciatura 2"]
                  },
                  {
                    "nivel_escolar":"Maestría",
                    "beca":["Ninguna","beca maestría 1", "beca maestría 2"]
                  }
                ],
                "active": true,
                "required": false,
                "id": "beca",
                "dependencia":"nivel_escolar"
              },
              {
                "pregunta": "Grupo sanguíneo",
                "type": "select",
                "options": ["A+", "B+", "O+","AB+", "A-", "B-", "O-","AB-"],
                "active": true,
                "required": false,
                "id": "tipo_sanguineo",
                "dependencia":""
              },
              {
                "pregunta": "¿Tiene alguna discapacidad?",
                "type": "select",
                "options": ["Ninguna","Visual","Auditiva","Fisica","Intelectual"],
                "active": true,
                "required": false,
                "id": "tipo_discapacidad",
                "dependencia": "",
              },
              {
                "pregunta": "¿Habla alguna lengua indígena?",
                "type": "select",
                "options": ["Ninguna","Nahuatl","Otomí"],
                "active": true,
                "required": false,
                "id": "lengua",
                "dependencia": ""
              },
              {
                "section":"Escuela de procedencia",
                "pregunta": "¿La escuela de procedencia es en México?",
                "type": "select",
                "options": ["Si","No"],
                "active": true,
                "required": false,
                "id": "procedencia_internacional",
                "dependencia":""
              },
              {
                "pregunta": "Nombre de la escuela",
                "type": "text",
                "options": [],
                "active": true,
                "required": false,
                "id": "nombre_escuela_internacional",
                "dependencia":"procedencia_internacional",
                "value":"No"
              },
              {
                "pregunta": "Dirección de la escuela",
                "type": "text",
                "options": [],
                "active": true,
                "required": false,
                "id": "direccion_escuela_internacional",
                "dependencia":"procedencia_internacional",
                "value":"No"
              },             
              {
                "type":"api",
                "nombre":"escuelas",
                "estado":"estado_procedencia",
                "municipio":"municipio_procedencia",
                "localidad":"localidad_procedencia",
                "escuela":"escuela_procedencia",
                "dependencia":"procedencia_internacional",
                "nivel": 9, /* 9 para secundaria o 6 para superior */
                "active":true,
                "value":"Si"
              },
              {
                "pregunta": "Fecha de ingreso",
                "type": "date",
                "options": [],
                "active": true,
                "required": false,
                "id": "ingreso",
                "dependencia":""
              },
              {
                "pregunta": "Fecha de egreso",
                "type": "date",
                "options": [],
                "active": true,
                "required": false,
                "id": "egreso",
                "dependencia":""
              },
              {
                "pregunta": "¿Cuál es tu promedio?",
                "type": "number",
                "options": [],
                "active": true,
                "required": true,
                "id": "promedio",
                "dependencia":""
              },
              {
                "pregunta": "¿Cuál fue la especialidad que curso?",
                "type": "text",
                "options": [],
                "active": true,
                "required": false,
                "id": "especialidad",
                "dependencia":""
              },
              {
                "pregunta": "¿Que tipo de sostenimiento tiene?",
                "type": "select",
                "options": ["Público","Privado"],
                "active": true,
                "required": false,
                "id": "sostenimiento",
                "dependencia":""
              },
              {
                "section":"Encuesta de impacto",
                "pregunta": "¿Menciona por qué medio conociste la institución?",
                "type": "select",
                "options": [
                  "Redes sociales",
                  "Triptico",
                  "Recomendaciones",
                  "Ferias de universidades",
                  "Visita guiada",
                  "Página Web",
                  "Promoción en tu escuela",
                  "Lonas",
                  "Trípticos",
                  "Radio",
                  "Folletos",
                  "Cartel",
                  "Amigos/alumnos",
                  "Otro"
                ],
                "active": true,
                "required": false,
                "id": "medio",
                "dependencia":""
              },
              {
                "pregunta": "¿Por qué decidiste ingresar a la institución?",
                "type": "select",
                "options": ["Plan de estudios","Ubicación","Instalaciones y laboratorios","Costos","Nivel Académico","Becas","Ofrecen las carreras que quiero","No tuve otra opción","Mis padres decidieron"],
                "active": true,
                "required": false,
                "id": "decision",
                "dependencia":""
              },
              {
                "pregunta": "¿Cómo calificas el servicio de promoción de la institución?",
                "type": "select",
                "options": ["Excelente","Bueno","Regular","Malo"],
                "active": true,
                "required": false,
                "id": "calificacion",
                "dependencia":""
              },
              {
                "pregunta": "¿Por qué medio te gustaría conocer más de la institución?",
                "type": "text",
                "options": [],
                "active": true,
                "required": false,
                "id": "otro_medio",
                "dependencia":""
              }
            ]
          },
          {
            "paso":"Documentos",
            "preguntas": [
              {
                "pregunta": "Acta de nacimiento",
                "type": "file",
                "options": [],
                "active": true,
                "required": false,
                "id":"acta"
              },
              {
                "pregunta": "Constancia o certificado de estudios",
                "type": "file",
                "options": [],
                "active": true,
                "required": false,
                "id":"constancia"
              },
              {
                "pregunta": "Carta de exposición de motivos (Solo mixta)",
                "type": "file",
                "options": [],
                "active": true,
                "required": false,
                "id":"carta_motivos"

              },
              {
                "pregunta": "Fotografía(con fondo blanco)",
                "type": "file",
                "options": [],
                "active": true,
                "required": false,
                "id":"fotografía"
              },
              {
                "pregunta": "CURP",
                "type": "file",
                "options": [],
                "active": false,
                "required": false,
                "id":"curp"
              },
              {
                "pregunta": "Carta de buena conducta",
                "type": "file",
                "options": [],
                "active": false,
                "required": false,
                "id":"carta_conducta"
              }
            ]
          },
          {
            "paso":"Datos de pago",
            "visualizacion_inicio": 1607493600000,
            "visualizacion_fin": 1607579999000,
            "bancos": [
              {
                "nombre": "Banamex",
                "referencia": "12345",
                "fecha_admision": "06/07/2020",
                "importe": "$1000.00 MXN",
                "dependencia": "nivel_escolar",
                "dependencia2":"promedio",
                "pago_en_linea": "url",
                "concepto": "Admision",
                "nivel_escolar":"Licenciatura",
                "promedio": 8

              },
              {
                "nombre": "Banamex",
                "referencia": "12345",
                "fecha_admision": "06/07/2020",
                "importe": "$2000.00 MXN",
                "dependencia": "nivel_escolar",
                "dependencia2":"promedio",
                "pago_en_linea": "url",
                "concepto": "Admision",
                "nivel_escolar":"Ingeniería",
                "promedio": 8

              },
              {
                "nombre": "Bancomer",
                "referencia": "12345",
                "fecha_admision": "06/07/2020",
                "importe": "$1111.00 MXN",
                "dependencia": "nivel_escolar",
                "dependencia2":"promedio",
                "pago_en_linea": "url",
                "concepto": "Admision",
                "nivel_escolar":"Maestría",
                "promedio": 8

              }
            ]
          },
          {
            "paso":"Pase",
            "fecha": "11/01/2020",
            "hora": "12:00 pm",
            "lugar": "En el lugar de costumbre",
            "instrucciones": "Favor de llevar lapiz goma y sacapuntas",
          }
        ]
      }
    ]  
}