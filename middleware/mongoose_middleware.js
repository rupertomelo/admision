const mongoose = require('mongoose');
const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');

mongoose.connect('mongodb://localhost:27017/admision');

module.exports = (app) => {
	const conn = mongoose.connection;

	conn.once('open', function () {
    
        const gfs = Grid(conn.db, mongoose.mongo);

    	const storage = new GridFsStorage({
            db : conn.db,
            file: (req, file) => {
                return {
                    uuid: req.params.userId,
                    filename: req.namefile
                };
            },
            cache: true
        });

        const upload = multer({
            storage: storage
        }).single('file');

        app.post('/v1/upload/:userId/:ext', (req, res, next) => {
            req.namefile = 'file_' + req.params.userId + "_" + Date.now() + "." + req.params.ext;
            next();
        }, upload, function(req, res) {
            res.send({ success: req.namefile });
        });

        app.get('/v1/get/:filename', function(req, res){
            gfs.files.find({filename: req.params.filename}).toArray(function(err, files){
                if(!files || files.length === 0){
                    return res.status(404).json({
                        responseCode: 1,
                        responseMessage: "error"
                    });
                }
                var readstream = gfs.createReadStream({
                    filename: files[0].filename
                });                
                res.set('Content-Type', files[0].contentType);
                return readstream.pipe(res);
            });
        });
	});
};