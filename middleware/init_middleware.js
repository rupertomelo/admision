const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const json2xls = require('json2xls');

const AllowAccessProvitional = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

module.exports = (app) => {
	app.use(AllowAccessProvitional);
	app.use(bodyParser.json({ limit: '50mb' }));
	app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
	app.use(cookieParser());
	app.use(json2xls.middleware);
	app.set('view engine', 'ejs');
};