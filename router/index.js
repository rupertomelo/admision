const secret = "4dm1s1oN";
const RegistroDB = require("../db/Registros");
const Admision = require("../db/Admision");
const Catalogo = require("../db/Catalogo");

const searchInstitucion = (request, response, next)=>{
	var institucion = request.params.institucion;
	Admision.formularios.forEach((item)=>{
		if(item.institucion == institucion){
			return response.search = item
		}
	});
	next();
}

module.exports = (app) => {
	app.get("/",(request,response) => {
		response.render('contents/index',{
			data:{
				title:"Inicio",
				admision: Admision
			}
		});
	});

	app.get('/step1/:institucion', searchInstitucion,(request, response)=>{
		response.render('contents/Registro',{
			data:{
				title: 'Registro',
				form: response.search,
				catalogo: Catalogo,
				institucion: request.params.institucion,
				user: {
					registro: "1"
				}
			}
		})
	})
}