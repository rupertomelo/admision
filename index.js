"use strict";

const port = 9330;
const express = require('express');
const app = express();

require("./middleware")(app);
require("./router")(app);
require("./private")(app);

app.listen(port, () => {
  console.log('Admision listening on port ', port, '!');
});